//
//  ViewController.swift
//  CodibilityTest
//
//  Created by Ahmed Elashker on 2/22/17.
//  Copyright © 2017 Ahmed Elashker. All rights reserved.
//

import UIKit

public class Tree {
    public var x : Int = 0
    public var l : Tree?
    public var r : Tree?
    public init() {}
}

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let A:[Int] = [-5,-3,-1,0,3,6]
        let B:[Int] = [4,5,9,10]
        let C:[Int] = [4,6,7,10,2]
        let N = 30
        let ff = test(M: 1, N: 1678)
//        let fstr = testStr(S: "racecar")
//        let tree1 = Tree()
//        let tree10 = Tree()
//        let tree21 = Tree()
//        let tree20 = Tree()
//        let tree5 = Tree()
//        let tree3 = Tree()
//        tree1.x = 1
//        tree10.x = 10
//        tree21.x = 21
//        tree20.x = 20
//        tree3.x = 3
//        tree5.x = 5
//        tree5.l = tree3
//        tree5.r = tree10
//        tree3.l = tree20
//        tree3.r = tree21
//        tree10.l = tree1
//        let testr = testTree(T: tree5)
    }
    
    func testTree(T:Tree) -> Int {
        func treeHeight(t: Tree) -> Int {
            var leftHeight = 0
            
            var rightHeight = 0
            
            if t.l != nil {
                leftHeight = 1 + treeHeight(t: t.l!)
            }
            
            if t.r != nil {
                rightHeight = 1 + treeHeight(t: t.r!)
            }
            
            return max(leftHeight, rightHeight)
        }
        
        return treeHeight(t: T)
    }
    
    func test(M:Int, N:Int) -> Int {
        
        if M == N || N == M+1 {
            return M ^ N
        }
        
        var results = Array(M...N)
        var iterator = 0
        while results.count > 1 {
            if iterator+1 >= results.count {
                iterator = 0
            }
            
            let tempM = results[iterator]
            let tempN = results[iterator+1]
            let bitxor = tempM ^ tempN
            results.remove(at: iterator+1)
            results.remove(at: iterator)
            results.insert(bitxor, at: iterator)
            
            iterator += 1
        }
        
        return results.first!
        
//        Below Is An Alternate Solution I was Pursuing Before Realizing That A Ready Made Operator
//        Already Exists In Swift For Performing Bitwise XOR, I Tested The Operator ^ On Given Values And It Produced
//        The Same Numbers
//
//        var str1 = String(M, radix:2)
//        var str2 = String(N, radix:2)
//        
//        let differenceOfLength = abs(str1.characters.count - str2.characters.count)
//        if str1.characters.count < str2.characters.count {
//            for _ in 0..<differenceOfLength {
//                str1.insert("0", at: str1.startIndex)
//            }
//        }
//        else if str2.characters.count < str1.characters.count {
//            for _ in 0..<differenceOfLength {
//                str2.insert("0", at: str1.startIndex)
//            }
//        }
//
    }
    
    func testStr(S:String) -> Int {

        return 1
    }
    
// MARK: Array Inversion Count
//    public func solution(_ A : inout [Int]) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        if 0 >= A.count-1 {
//            return 0
//        }
//        
//        var inversionCount = 0
//        for i in 0..<A.count-1 {
//            for j in i+1..<A.count {
//                if A[i] > A[j] {
//                    inversionCount += 1
//                }
//            }
//        }
//        return inversionCount
//    }
    
// MARK: Binary Tree Height
//    public func solution(_ T : Tree?) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        func treeHeight(t: Tree) -> Int {
//            var leftHeight = 0
//            
//            var rightHeight = 0
//            
//            if t.l != nil {
//                leftHeight = 1 + treeHeight(t: t.l!)
//            }
//            
//            if t.r != nil {
//                rightHeight = 1 + treeHeight(t: t.r!)
//            }
//            
//            return max(leftHeight, rightHeight)
//        }
//        
//        return treeHeight(t: T!)
//    }
    
// MARK: Symmetric String
//    public func solution(_ S : inout String) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        if S.characters.count == 0 {
//            return -1
//        }
//        
//        let mid = S.characters.count / 2
//        
//        let left = S[S.startIndex..<S.index(S.startIndex, offsetBy: mid)]
//        
//        let right = String(S[S.index(S.startIndex, offsetBy: mid+1)..<S.endIndex].characters.reversed())
//        
//        if S.characters.count == 1 {
//            return 0
//        }
//        
//        if left != right {
//            return -1
//        }
//        
//        return mid
//    }
    
// MARK: Socks Laundering
//    public func solution(_ K : Int, _ C : inout [Int], _ D : inout [Int]) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        var clean = 0
//        var canRund = K
//        var unfair:[Int] = []
//        for i in C {
//            if (unfair.contains(i)) {
//                unfair.remove(at: unfair.index(of: i)!)
//                clean += 1
//            }
//            else {
//                unfair.append(i)
//            }
//        }
//        var dirty = D
//        if(canRund > 0) {
//            for i in unfair {
//                if(dirty.contains(i)) {
//                    dirty.remove(at: dirty.index(of: i)!)
//                    clean += 1
//                    canRund -= 1
//                    
//                    if(canRund == 0) {
//                        return clean
//                    }
//                }
//            }
//            var i = 0
//            while(canRund > 1 && i < dirty.count)
//            {
//                if dirty[0...i].contains(dirty[i]) {
//                    dirty.remove(at: dirty.index(of: dirty[i])!)
//                    dirty.remove(at: dirty.index(of: dirty[i])!)
//                    clean += 1
//                    canRund -= 2
//                }
//                else {
//                    i += 1
//                }
//            }
//        }
//        return clean
//    }
    
// MARK: Tennis Tournament
//    public func solution(_ P : Int, _ C : Int) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        return C*2 < P ? C : P/2
//    }
    
// MARK: Dwarfs Rafting
//    public func solution(_ N : Int, _ S : inout String, _ T : inout String) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        let barrels = S.characters.split(separator: " ")
//        let dwarfs = T.characters.split(separator: " ")
//        
//        var barrelsPoints:[CGPoint] = []
//        for b in barrels {
//            barrelsPoints.append(raftPositionToPoint(pos: String(b)))
//        }
//        
//        var dwarfsPoints:[CGPoint] = []
//        for d in dwarfs {
//            dwarfsPoints.append(raftPositionToPoint(pos: String(d)))
//        }
//        
//        let frontEnd:CGFloat = CGFloat(N)/2
//        
//        // left front from (1,1) to (frontend, frontend)
//        // right front from (1,backstart) to (frontend,N)
//        // left back from (backstart,1) to (N,frontend)
//        // right back from (backstart,backstart) to (N,N)
//        
//        var leftFront:[CGPoint] = []
//        var rightFront:[CGPoint] = []
//        var leftBack:[CGPoint] = []
//        var rightBack:[CGPoint] = []
//        
//        for point in dwarfsPoints {
//            if point.x >= 1 && point.x <= frontEnd {
//                // Front
//                if point.y >= 1 && point.y <= frontEnd {
//                    leftFront.append(point)
//                }
//                else {
//                    rightFront.append(point)
//                }
//            }
//            else {
//                // Back
//                if point.y >= 1 && point.y <= frontEnd {
//                    leftBack.append(point)
//                }
//                else {
//                    rightBack.append(point)
//                }
//            }
//        }
//        
//        let leftFrontDwarfs = leftFront.count
//        let rightFrontDwarfs = rightFront.count
//        let leftBackDwarfs = leftBack.count
//        let rightBackDwarfs = rightBack.count
//        
//        for point in barrelsPoints {
//            if point.x >= 1 && point.x <= frontEnd {
//                // Front
//                if point.y >= 1 && point.y <= frontEnd {
//                    leftFront.append(point)
//                }
//                else {
//                    rightFront.append(point)
//                }
//            }
//            else {
//                // Back
//                if point.y >= 1 && point.y <= frontEnd {
//                    leftBack.append(point)
//                }
//                else {
//                    rightBack.append(point)
//                }
//            }
//        }
//        
//        let sectionSize = (N * N) / 4
//        
//        let leftFrontNewDwarfsCount = sectionSize - leftFront.count + leftFrontDwarfs
//        let rightFrontNewDwarfsCount = sectionSize - rightFront.count + rightFrontDwarfs
//        let leftBackNewDwarfsCount = sectionSize - leftBack.count + leftBackDwarfs
//        let rightBackNewDwarfsCount = sectionSize - rightBack.count + rightBackDwarfs
//        
//        let minimalDwarfsInASection = min(leftFrontNewDwarfsCount, rightFrontNewDwarfsCount, leftBackNewDwarfsCount, rightBackNewDwarfsCount)
//        
//        if leftFrontDwarfs > minimalDwarfsInASection || rightBackDwarfs > minimalDwarfsInASection || leftBackDwarfs > minimalDwarfsInASection || rightBackDwarfs > minimalDwarfsInASection {
//            return -1
//        }
//        
//        var leftFrontSpaces = 0
//        var rightFrontSpaces = 0
//        var leftBackSpaces = 0
//        var rightBackSpaces = 0
//        var minimum = -1
//        
//        if sectionSize == minimalDwarfsInASection {
//            leftFrontSpaces = sectionSize - leftFrontDwarfs
//            rightFrontSpaces = sectionSize - rightFrontDwarfs
//            leftBackSpaces = sectionSize - leftBackDwarfs
//            rightBackSpaces = sectionSize - rightBackDwarfs
//            
//            minimum = leftFrontSpaces + rightFrontSpaces + leftBackSpaces + rightBackSpaces
//        }
//        else {
//            
//            leftFrontSpaces = sectionSize - leftFront.count
//            rightFrontSpaces = sectionSize - rightFront.count
//            leftBackSpaces = sectionSize - leftBack.count
//            rightBackSpaces = sectionSize - rightBack.count
//            
//            let frontMin = leftFrontSpaces + rightFrontSpaces
//            let backMin = leftBackSpaces + rightBackSpaces
//            let leftMin = leftFrontSpaces + leftBackSpaces
//            let rightMin = rightFrontSpaces + rightBackSpaces
//            
//            minimum = min(frontMin, backMin, leftMin, rightMin) * 2
//        }
//        
//        if minimum < 1 {
//            minimum = -1
//        }
//        return minimum
//    }
//    
//    let alpha = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q",
//                 "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
//    
//    func raftPositionToPoint(pos:String) -> CGPoint {
//        let x = Int(String(describing: pos.characters.first!))!
//        let y = alpha.index(of: String(pos.characters.last!))! + 1
//        return CGPoint(x: CGFloat(x), y: CGFloat(y))
//    }
    
// MARK: Rectangle Builder Count Ways Of Building
//    public func solution(_ A : inout [Int], _ X : Int) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        if A.count < 4 {
//            return 0
//        }
//        
//        var tempA = A
//        var ways = 0
//        
//        var tempB:[Int] = [] // To hold single occuring values
//        for e in A {
//            if tempB.contains(e) {
//                tempB.remove(at: tempB.index(of: e)!)
//            }
//            else {
//                tempB.append(e)
//            }
//        }
//        
//        for e in tempB {
//            tempA.remove(at: tempA.index(of: e)!) // tempA now free of single occuring values
//        }
//        
//        var i = 0
//        var j = 1
//        tempA.sort()
//        
//        var points:[CGPoint] = []
//        
//        while true {
//            if tempA.count < 4 {
//                break
//            }
//            
//            if (tempA[i] * tempA[j]) >= X {
//                let p = CGPoint(x: CGFloat(tempA[i]), y: CGFloat(tempA[j]))
//                if !points.contains(p) {
//                    var tempC = tempA
//                    tempC.remove(at: tempC.index(of: tempA[i])!)
//                    tempC.remove(at: tempC.index(of: tempA[j])!)
//                    if tempC.contains(tempA[i]) && tempC.contains(tempA[j]) {
//                        points.append(p)
//                        ways += 1
//                    }
//                }
//            }
//            
//            if j < tempA.count-1 {
//                j += 1
//            }
//            else {
//                if i < tempA.count-2 {
//                    i += 1
//                    j = i+1
//                }
//                else {
//                    break
//                }
//            }
//        }
//        
//        if ways > 1000000000 {
//            return -1
//        }
//        return ways
//    }
    
// MARK: Flood Depth
//    public func solution(_ A : inout [Int]) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        var b = 0
//        var c = 0
//        var maximum = 0
//        
//        for i in 1..<A.count {
//            if (A[i] > A[b]) {
//                maximum = max(A[b] - A[c], maximum);
//                b = i
//                c = b
//            } else if (A[i] > A[c]) {
//                maximum = max(A[i] - A[c], maximum);
//            } else if (A[i] < A[c]) {
//                c = i;
//            }
//        }
//        
//        return maximum
//    }
    
// MARK: Longest Password
//    public func solution(_ S : inout String) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        let words = S.characters.split(separator: " ")
//        var max = -1
//        for w in words {
//            if (isValid(str: String(w)) && w.count > max) {
//                max = w.count
//            }
//        }
//        return max
//    }
//    
//    func isValid(str:String) -> Bool {
//        let letters = CharacterSet.letters
//        let digits = CharacterSet.decimalDigits
//        
//        var letterCount = 0
//        var digitCount = 0
//        
//        for uni in str.unicodeScalars {
//            if letters.contains(uni) {
//                letterCount += 1
//            } else if digits.contains(uni) {
//                digitCount += 1
//            }
//        }
//        
//        if str.characters.count != letterCount + digitCount {
//            return false
//        }
//        
//        if digitCount % 2 != 1 {
//            return false
//        }
//        
//        return letterCount % 2 == 0
//    }
    
// MARK: Minimum Absolute Sum Of Elements
//    public func solution(_ A : inout [Int]) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        var maximum = 0
//        var sum = 0
//        var tempA = A
//        
//        for i in 0..<A.count {
//            tempA[i] = abs(tempA[i]);
//            maximum = maximum < tempA[i] ? tempA[i] : maximum
//            sum += tempA[i];
//        }
//        
//        var dp:[Int] = [Int](repeating:0, count:sum+1)
//        dp[0] = 1
//        for i in 0..<tempA.count {
//            for j in stride(from: sum, through: 0, by: -1) {
//                if (dp[j] == 1 && j + tempA[i] <= sum){
//                    dp[j + tempA[i]] = 1;
//                }
//            }
//        }
//        
//        var result = sum
//        var p = 0
//        while p*2 <= sum {
//            if (dp[p] == 1){
//                let q = sum - p
//                let diff = q - p
//                result = diff < result ? diff : result;
//            }
//            p += 1
//        }
//        return result
//    }
    
// MARK: Number Solitaire
//    public func solution(_ A : inout [Int]) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        let n = A.count;
//        var dp = [Int](repeating:0, count:n)
//        dp[0] = A[0]
//        for i in 1..<n {
//            var maximum = dp[i - 1] + A[i]
//            for j in 1..<7 {
//                if (i - j >= 0) {
//                    maximum = max(dp[i - j] + A[i], maximum)
//                }
//            }
//            dp[i] = maximum
//        }
//        return dp[n - 1]
//    }
    
// MARK: Maximum Non Overlapping Segments
//    public func solution(_ A : inout [Int], _ B : inout [Int]) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        if (A.count == 0) {
//            return A.count;
//        }
//        
//        var count = 1
//        var ending = B[0]
//        for i in 1..<B.count {
//            if (A[i] > ending) {
//                ending = B[i]
//                count += 1
//            }
//        }
//        
//        return count
//    }
    
// MARK: Tie Ropes
//    public func solution(_ K : Int, _ A : inout [Int]) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        var sum = 0
//        var ropes = 0
//        for e in A {
//            sum += e
//            if (sum >= K) {
//                sum = 0
//                ropes += 1
//            }
//        }
//        
//        return ropes
//    }
    
// MARK: Minimum Absolute Sum Of Two
//    public func solution(_ A : inout [Int]) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        var tempA = A
//        tempA.sort()
//        var tail = tempA.count - 1
//        var minimum = abs(tempA[0] + tempA[tail])
//        
//        for head in 0..<tail+1 {
//            while (head <= tail) {
//                let value = abs(tempA[head] + tempA[tail])
//                let next = abs(tempA[head] + tempA[max(0, tail - 1)])
//                
//                minimum = min(value, minimum)
//                
//                if (next <= value) {
//                    tail -= 1
//                } else {
//                    break
//                }
//            }
//        }
//        
//        return minimum
//    }
    
// MARK: Count Triangles
//    public func solution(_ A : inout [Int]) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        if (A.count < 3) {
//            return 0
//        }
//        
//        var r = 0
//        var count = 0
//        var tempA = A
//        tempA.sort()
//        
//        for p in 0..<tempA.count-2 {
//            r = p + 2
//            for q in p+1..<tempA.count {
//                while (r < tempA.count && ((tempA[p] + tempA[q] > tempA[r])
//                    && (tempA[q] + tempA[r] > tempA[p])
//                    && (tempA[r] + tempA[p] > tempA[q]))) {
//                        r += 1
//                }
//                count += r - q - 1
//            }
//        }
//        
//        return count
//    }
    
// MARK: Count Distinct Slices
//    public func solution(_ M : Int, _ A : inout [Int]) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        var i = 0
//        var j = 0
//        var distinctSlicesCount = 0
//        while i < A.count {
//            let slice = A[i...j]
//            let arrayCount = slice.count
//            let setCount = Set(slice).count
//            if setCount == arrayCount {
//                distinctSlicesCount += 1
//            }
//            
//            if j < A.count-1 {
//                j += 1
//            }
//            else {
//                i += 1
//                j = i
//            }
//        }
//        return distinctSlicesCount
//    }
    
// MARK: Absolute Distinct Count
//    public func solution(_ A : inout [Int]) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        var tempA = A
//        for i in 0..<tempA.count {
//            if A[i] < 0 {
//                tempA[i] = tempA[i] * -1
//            }
//        }
//        return Set(tempA).count
//    }
    
// MARK: Minimal Large Sum
//    public func solution(_ K : Int, _ M : Int, _ A : inout [Int]) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        var l = A.max()!
//        var tempM = l * A.count
//        
//        while (l < tempM)
//        {
//            let t = (l + tempM) / 2;
//            
//            var i = 0, j = 0, s = 0
//            while (i < A.count && j < K)
//            {
//                s += A[i]
//                if (s > t)
//                {
//                    s = A[i]
//                    j += 1
//                }
//                i += 1
//            }
//            
//            if (j == K) {
//                l = t + 1
//            }
//            else {
//                tempM = t
//            }
//            
//        }
//        return tempM
//    }
    
// MARK: Nailing Planks
//    public func solution(_ A : inout [Int], _ B : inout [Int], _ C : inout [Int]) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        let N = A.count
//        let M = C.count
//        let nailed_size = MemoryLayout<Int>.size * N
//        let nailed:[Int] = [Int](repeating:0, count:nailed_size)
//        
//        var beg = 1
//        var end = M
//        
//        var min = M + 1
//        while(beg <= end) {
//            let mid = (beg + end) / 2
//            if check(k: mid, nailed: nailed, A: A, B: B, N: N, C: C, M: M) {
//                end = mid - 1
//                min = mid
//            }
//            else {
//                beg = mid + 1
//            }
//        }
//        
//        return min == M + 1 ? -1 : min
//    }
//    
//    func check(k:Int, nailed:[Int], A:[Int], B:[Int], N:Int, C:[Int], M:Int) -> Bool {
//        var tempNailed = nailed
//        for i in 0..<k {
//            for j in 0..<N {
//                if A[j] <= C[i] && C[i] <= B[j] {
//                    tempNailed[j] = 1
//                }
//            }
//        }
//        
//        for i in 0..<N {
//            if tempNailed[i] == 0 {
//                return false
//            }
//        }
//        return true
//    }
    
// MARK: Fib Frog
//    public func solution(_ A : inout [Int]) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        var fib:[Int] = []
//        fib.append(0)
//        fib.append(1)
//        
//        while true {
//            let f = fib[fib.count-1] + fib[fib.count-2]
//            fib.append(f)
//            if (f > A.count) {
//                break
//            }
//        }
//        
//        fib.reverse()
//        
//        var index = 0
//        let N = A.count
//        var que:[CGPoint] = []
//        que.append(CGPoint(x: -1, y: 0))
//        
//        var tempA = A
//        while true {
//            if (index == que.count) {
//                return -1
//            }
//            let current = que[index]
//            for k in fib {
//                let next = k + Int(current.x)
//                if (next == N) {
//                    return Int(current.y) + 1
//                }
//                if ((next > N) || next < 0 || (A[next] == 0)) {
//                    continue
//                }
//                let point = CGPoint(x: CGFloat(next), y: CGFloat(Int(current.y)+1))
//                que.append(point)
//                tempA[next] = 0
//            }
//            index += 1
//        }
//    }
    
// MARK: Ladder
//    public func solution(_ A : inout [Int], _ B : inout [Int]) -> [Int] {
//        // write your code in Swift 3.0 (Linux)
//        var maximum = 0
//        for i in 0..<A.count {
//            maximum = max(A[i], maximum)
//        }
//        
//        maximum += 2
//        
//        var fib:[Double] = [Double](repeating:0, count: maximum)
//        
//        fib[0] = 1
//        fib[1] = 1
//        for i in 2..<maximum {
//            fib[i] = (fib[i - 1] + fib[i - 2]).truncatingRemainder(dividingBy: Double(1 << 30))
//        }
//        
//        var res:[Int] = [Int](repeating:0, count:A.count)
//        for i in 0..<A.count {
//            res[i] = Int(fib[A[i]].truncatingRemainder(dividingBy: Double((1 << B[i]))))
//        }
//        
//        return res
//    }
    
// MARK: Common Prime Factors
//    public func solution(_ A : inout [Int], _ B : inout [Int]) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        var commonDivs = 0
//        for i in 0..<A.count {
//            if primeFactors(n: A[i]) == primeFactors(n: B[i]) {
//                commonDivs += 1
//            }
//        }
//        return commonDivs
//    }
//    
//    func primeFactors(n: Int) -> [Int] {
//        
//        if n == 1 {
//            return [1]
//        }
//        
//        var i = 2
//        var divisors:[Int] = []
//        while i < n+1 {
//            if n % i == 0 {
//                divisors.append(i)
//            }
//            i += 1
//        }
//        
//        var x = 0
//        var y = 1
//        while true {
//            var holdY = false
//            if divisors.count > 1 {
//                if divisors[y] % divisors[x] == 0 {
//                    divisors.remove(at: divisors.index(of: divisors[y])!)
//                    holdY = true
//                }
//                if !holdY {
//                    if y < divisors.count-1 {
//                        
//                        y += 1
//                        
//                    }
//                    else {
//                        if x < divisors.count-2 {
//                            x += 1
//                            y = x + 1
//                        }
//                        else {
//                            break
//                        }
//                    }
//                }
//                else {
//                    x = 0
//                    y = 1
//                }
//            }
//            else {
//                break
//            }
//        }
//        
//        return divisors
//    }
    
// MARK: Chocolate Circules
//    public func solution(_ N : Int, _ M : Int) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        if M == N {
//            return 1
//        }
//        var i = 0
//        var setArr:[Int] = [i]
//        while true {
//            i += M
//            if i >= N {
//                while i >= N {
//                    i -= N
//                }
//            }
//            
//            let preEatCount = Set(setArr).count
//            setArr.append(i)
//            if Set(setArr).count == preEatCount {
//                break
//            }
//        }
//        return Set(setArr).count
//    }
    
// MARK: Count Non Divisables
//    public func solution(_ A : inout [Int]) -> [Int] {
//        // write your code in Swift 3.0 (Linux)
//        if A.count == 1 {
//            return [0]
//        }
//        
//        var nonDivisorsCount:[Int] = []
//        for i in 0..<A.count {
//            var iNonDivisors = 0
//            for j in 0..<A.count {
//                if A[i] % A[j] != 0 {
//                    iNonDivisors += 1
//                }
//            }
//            nonDivisorsCount.append(iNonDivisors)
//        }
//        return nonDivisorsCount
//    }
    
// MARK: Semiprimes Count
//    public func solution(_ N : Int, _ P : inout [Int], _ Q : inout [Int]) -> [Int] {
//        // write your code in Swift 3.0 (Linux)
//        let m = P.count;
//        var M:[Int] = [Int](repeating:0 ,count:m)
//        
//        var f:[Int] = [Int](repeating:0 ,count:N+1)
//        var i = 2
//        while (i * i <= N) {
//            if (f[i] == 0) {
//                var k = i * i
//                while (k <= N) {
//                    if (f[k] == 0) {
//                        f[k] = i
//                    }
//                    k += i
//                }
//            }
//            i += 1
//        }
//        
//        var semi = [Int](repeating:0 ,count:N+1)
//        
//        var sum = 0
//        for k in 1..<N+1 {
//            if (f[k] != 0) {
//                let b = k / f[k]
//                if (f[b] == 0) {
//                    sum += 1
//                }
//            }
//            semi[k] = sum;
//        }
//        
//        for mi in 0..<m {
//            let p = P[mi]
//            let q = Q[mi]
//            M[mi] = semi[q] - semi[p - 1]
//        }
//        
//        return M
//    }
    
// MARK: Flags
//    public func solution(_ A : inout [Int]) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        if A.count < 3 {
//            return 0
//        }
//        
//        var peaks = [Int]()
//        for i in 1..<(A.count - 1) {
//            if A[i - 1] < A[i] && A[i] > A[i + 1] {
//                peaks.append(i)
//            }
//        }
//        if peaks.count < 2 {
//            return peaks.count
//        }
//        
//        var flags = 2
//        var maxFlagsCount = 2
//        
//        let maxDistance = peaks.last! - peaks.first!
//        let maxFlags = Int(sqrt(Double(maxDistance))) + 1
//        
//        for f in flags...maxFlags {
//            var i = 1
//            var peakCount = 1
//            var lastPeak = peaks[0]
//            while i < peaks.count && peakCount < f {
//                if peaks[i] - lastPeak >= f {
//                    peakCount += 1
//                    lastPeak = peaks[i]
//                }
//                i += 1
//            }
//            if f == peakCount {
//                maxFlagsCount = max(maxFlagsCount, f)
//            }
//        }
//        
//        return maxFlagsCount
//    }
    
// MARK: Peaks
//    public func solution(_ A : inout [Int]) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        let n = A.count
//        if n == 1 {
//            return 0
//        }
//        var peaks:[Int] = []
//        for i in 1..<n-1 {
//            if (A[i - 1] < A[i] && A[i] > A[i + 1]) {
//                peaks.append(i)
//            }
//        }
//        
//        var max = 0
//        for i in 1..<n {
//            if ((n % i) == 0) {
//                var bi = 0
//                let block = n / i
//                for p in peaks {
//                    if (bi * block <= p && p < (bi + 1) * block) {
//                        bi += 1
//                    }
//                }
//                if (bi == i) {
//                    max = i
//                }
//            }
//        }
//        
//        return max
//    }

// MARK: Minimum Perimeter Rectangle
//    public func solution(_ N : Int) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        var i = 1
//        var minimum = 2 * (N + 1)
//        while (i * i < N) {
//            if ((N % i) == 0) {
//                let per = 2 * (i + N / i)
//                minimum = min(minimum, per)
//            }
//            i += 1
//        }
//        if (i * i == N) {
//            let per = 2 * (i + i)
//            minimum = min(minimum, per)
//        }
//        return minimum
//    }
    
// MARK: Count Factor
//    public func solution(_ N : Int) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        var sum = 0
//        var i = 1
//        
//        while (i * i < N) {
//            if ((N % i) == 0) {
//                sum += 2
//            }
//            i += 1
//        }
//        
//        if (i * i == N) {
//            sum += 1
//        }
//        
//        return sum
//    }
    
// MARK: Maximum Slice Sum
//    public func solution(_ A : inout [Int]) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        var K:[Int] = [Int](repeating:0 ,count:A.count)
//        K[0] = A[0]
//        for i in 1..<A.count {
//            let s = max(K[i - 1], 0) + A[i]
//            K[i] = s
//        }
//        K.sort()
//        return K[K.count - 1]
//    }

// MARK: Maximum Profit
//    public func solution(_ A : inout [Int]) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        if (A.count < 2) {
//            return 0
//        }
//        var msf = 0
//        var meh = 0
//        for i in 1..<A.count {
//            meh = max(0, meh + A[i] - A[i - 1])
//            msf = max(msf, meh)
//        }
//        return msf > 0 ? msf : 0
//    }
    
// MARK: Maximum Double Slice Sum
//    public func solution(_ A : inout [Int]) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        var max_slice = 0
//        var max_ending = 0
//        var y = 1
//        var temp1 = 0
//        var temp2 = 0
//        if (A.count == 3) {
//            return 0
//        }
//        for i in 3..<A.count {
//            temp1 = max_ending + A[i-1];
//            temp2 = max_ending + A[y];
//            if ((temp1 > 0) && (temp1 >= temp2)) {
//                max_ending = temp1
//            }
//            else if ((temp2 > 0) && (temp2 >= temp1)) {
//                max_ending = temp2
//                y = i - 1
//            }
//            else {
//                max_ending = 0
//            }
//            max_slice = max(max_slice, max_ending)
//        }
//        return max_slice
//    }
    
// MARK: Equi Leader
//    public func solution(_ A : inout [Int]) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        var leader = -1
//        var countOfLeader = 0
//        var tempArr:[Int] = []
//        for i in 0..<A.count {
//            if tempArr.count == 0 || tempArr.max() == A[i] {
//                tempArr.append(A[i])
//            }
//            else {
//                tempArr = Array(tempArr.dropLast())
//            }
//        }
//        if tempArr.count != 0 {
//            for i in 0..<A.count {
//                if A[i] == tempArr.max() {
//                    countOfLeader += 1
//                }
//            }
//            if countOfLeader > A.count/2 {
//                leader = tempArr.max()!
//            }
//            else {
//                return 0
//            }
//        }
//        else {
//            return 0
//        }
//        var countEqui = 0
//        var countOfLeaderInRight = 0
//        var countOfLeaderInLeft = countOfLeader
//        for i in stride(from: A.count-1, through: 0, by: -1) {
//            if A[i] == leader {
//                countOfLeaderInRight += 1
//                countOfLeaderInLeft -= 1
//            }
//            if countOfLeaderInLeft > i/2 && countOfLeaderInRight > (A.count - i)/2 {
//                countEqui += 1
//            }
//        }
//        return countEqui
//    }
    
// MARK: Dominator
//    public func solution(_ A : inout [Int]) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        if A.count < 1 {
//            return -1
//        }
//        let set = Set(A)
//        var keys:[Int] = []
//        for item in set {
//            keys.append(item)
//        }
//        var vals:[Int] = [Int](repeating: 0, count: set.count)
//        for e in A {
//            let index = keys.index(of: e)
//            vals[index!] += 1
//        }
//        if A.count/2 < vals.max()! {
//            return A.index(of: keys[vals.index(of: vals.max()!)!])!
//        }
//        return -1
//    }
    
// MARK: Fish Staying Alive
//    public func solution(_ A : inout [Int], _ B : inout [Int]) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        var fishSize:[Int] = []
//        var fishDir:[Int] = []
//        for i in 0..<A.count {
//            if B[i] == 1 {
//                fishSize.append(A[i])
//                fishDir.append(B[i])
//            }
//            else {
//                while true {
//                    if fishSize.count == 0 {
//                        fishSize.append(A[i])
//                        fishDir.append(B[i])
//                        break
//                    }
//                    else if fishDir.max() == 1 {
//                        if fishSize.max()! > A[i] {
//                            break
//                        }
//                        else if fishSize.max()! < A[i] {
//                            fishSize = Array(fishSize.dropLast())
//                            fishDir = Array(fishDir.dropLast())
//                        }
//                    }
//                    else {
//                        fishSize.append(A[i])
//                        fishDir.append(B[i])
//                        break
//                    }
//                }
//            }
//        }
//        return fishSize.count
//    }
    
// MARK: Stone Wall
//    public func solution(_ H : inout [Int]) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        var temp = 0
//        var count = 0
//        var tempArr:[Int] = []
//        for e in H {
//            if e > temp {
//                count += 1
//                tempArr.append(e)
//            }
//            else if e < temp {
//                while true {
//                    if tempArr.count == 0 {
//                        tempArr.append(e)
//                        count += 1
//                        break
//                    }
//                    else if tempArr.max()! > e {
//                        tempArr = Array(tempArr.dropLast())
//                    }
//                    else if tempArr.max()! == e {
//                        break
//                    }
//                    else if tempArr.max()! < e {
//                        tempArr.append(e)
//                        count += 1
//                        break
//                    }
//                }
//            }
//            temp = e
//        }
//        return count
//    }
    
// MARK: Brackets & Nesting
//    public func solution(_ S : inout String) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        var curlyClue = 0
//        var squareClue = 0
//        var bracketClue = 0
//        var lastOpened = ""
//        if S.isEmpty {
//            return 1
//        }
//        for ch in S.characters {
//            if ch == "{" {
//                curlyClue += 1
//                lastOpened += "c"
//            }
//            else if ch == "[" {
//                squareClue += 1
//                lastOpened += "s"
//            }
//            else if ch == "(" {
//                bracketClue += 1
//                lastOpened += "b"
//            }
//            else if ch == "}" {
//                if lastOpened.characters.last != "c" {
//                    return 0
//                }
//                curlyClue -= 1
//                lastOpened = String(lastOpened.characters.dropLast())
//            }
//            else if ch == "]" {
//                if lastOpened.characters.last != "s" {
//                    return 0
//                }
//                squareClue -= 1
//                lastOpened = String(lastOpened.characters.dropLast())
//            }
//            else if ch == ")" {
//                if lastOpened.characters.last != "b" {
//                    return 0
//                }
//                bracketClue -= 1
//                lastOpened = String(lastOpened.characters.dropLast())
//            }
//        }
//        
//        if curlyClue + bracketClue + squareClue == 0 {
//            return 1
//        }
//        return 0
//    }
    
// MARK: Disc Intersections
//    public func solution(_ A : inout [Int]) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        if A.count < 2 {
//            return 0
//        }
//        
//        if A.count == 2 && A.first == A.last {
//            return 1
//        }
//        
//        var intersections = 0
//        
//        var sets = [Set<Int64>]()
//        for i in 0..<A.count {
//            if A[i] == 2147483647 {
//                intersections += A.count - 1
//            }
//            else {
//                let arr = Array(i.toIntMax()-A[i].toIntMax()...i.toIntMax()+A[i].toIntMax())
//                sets.append(Set(arr))
//            }
//        }
//        
//        var i = 0
//        var j = 1
//        
//        if sets.count == 0 {
//            return intersections
//        }
//        
//        if sets[i].intersection(sets[j]).count > 0 {
//            intersections += 1
//        }
//        
//        while i < sets.count-2 {
//            if j < sets.count-1 {
//                j += 1
//            }
//            else {
//                i += 1
//                j = i + 1
//            }
//            
//            if sets[i].intersection(sets[j]).count > 0 {
//                intersections += 1
//            }
//        }
//        
//        if intersections > 10000000 {
//            return -1
//        }
//        
//        return intersections
//    }
    
// MARK: Triangle
//    public func solution(_ A : inout [Int]) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        if A.count < 3 {
//            return 0
//        }
//        
//        var i = 0
//        var j = 1
//        var k = 2
//        
//        if A[i] + A[j] > A[k] {
//            if A[i] + A[k] > A[j] {
//                if A[k] + A[j] > A[i] {
//                    return 1
//                }
//            }
//        }
//        
//        while i < A.count - 3 {
//            if k < A.count-1 {
//                k += 1
//            }
//            else {
//                if j < A.count-2 {
//                    j += 1
//                    k = A.count-(A.count-2-j)-(A.count-1-k)-1
//                }
//                else {
//                    if i < A.count-3 {
//                        i += 1
//                    }
//                }
//            }
//            
//            if A[i] + A[j] > A[k] {
//                if A[i] + A[k] > A[j] {
//                    if A[k] + A[j] > A[i] {
//                        return 1
//                    }
//                }
//            }
//        }
//        return 0
//    }

// MARK: Maximum Product Of 3
//    public func solution(_ A : inout [Int]) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        var maximumValue = A[0] * A[1] * A[2]
//        var i = 0
//        var j = 1
//        var k = 2
//        while i < A.count - 3 {
//            let nextMax = A[i] * A[j] * A[k]
//            if nextMax > maximumValue {
//                maximumValue = nextMax
//            }
//            if k != A.count-1 {
//                k += 1
//            }
//            else {
//                if j != A.count-2 {
//                    j += 1
//                }
//                else {
//                    if i != A.count-3 {
//                        i += 1
//                    }
//                }
//            }
//        }
//        return maximumValue
//    }
    
// MARK: Distinct Values
//    public func solution(_ A : inout [Int]) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        
//        let set = Set(A)
//        return set.count
//    }

// MARK: Minimum Slice Index
//    public func solution(_ A : inout [Int]) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        var minimumIndex = 0
//        var minimumVal = Float((A.first! + A[1]) / 2)
//        for i in 0..<A.count-1 {
//            for j in i+1..<A.count {
//                let slice = A[i..<j+1]
//                let avg:Float = Float(slice.reduce(0, +)) / Float(slice.count)
//                if avg < minimumVal {
//                    minimumVal = avg
//                    minimumIndex = i
//                }
//            }
//        }
//        return minimumIndex
//    }
    
// MARK: DNA Smallest Impact
//    public func solution(_ S : inout String, _ P : inout [Int], _ Q : inout [Int]) -> [Int] {
//        // write your code in Swift 3.0 (Linux)
//        var numbersArray:[Int] = []
//        for ch in S.characters {
//            if ch == "A" {
//                numbersArray.append(1)
//            }
//            else if ch == "C" {
//                numbersArray.append(2)
//            }
//            else if ch == "G" {
//                numbersArray.append(3)
//            }
//            else if ch == "T" {
//                numbersArray.append(4)
//            }
//        }
//        
//        var smallestImpacts:[Int] = []
//        for i in 0..<P.count {
//            let startIndex = P[i]
//            let endIndex = Q[i]
//            let subset = numbersArray[startIndex..<endIndex+1]
//            smallestImpacts.append(subset.count == 0 ? numbersArray[P[i]] : subset.min()!)
//        }
//        
//        return smallestImpacts
//    }
    
// MARK: Passing Cars
//    public func solution(_ A : inout [Int]) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        var numbers = 0
//        for i in 0..<A.count {
//            for j in i+1..<A.count {
//                if A[i] == 0 && A[j] == 1 {
//                    numbers += 1
//                }
//            }
//        }
//        
//        if numbers > 1000000000 {
//            return -1
//        }
//        
//        return numbers
//    }
    
// MARK: Count Div
//    public func solution(_ A : Int, _ B : Int, _ K : Int) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        if A == 0 && B == 0 {
//            return 1
//        }
//        
//        var numbers = 0
//        var tempA = A
//        while tempA < B + 1 {
//            if tempA % K == 0 {
//                numbers += 1
//            }
//            tempA += 1
//        }
//        
//        return numbers
//    }
    
// MARK: Counters
//    public func solution(_ N : Int, _ A : inout [Int]) -> [Int] {
//        // write your code in Swift 3.0 (Linux)
//        var counters:[Int] = [Int](repeating: 0, count: N)
//        for e in A {
//            if e == N + 1 {
//                counters = [Int](repeating: counters.max()!, count: N)
//            }
//            else {
//                counters[e-1] += 1
//            }
//        }
//        
//        return counters
//    }
    
// MARK: Earliest Frog Jump
//    public func solution(_ X : Int, _ A : inout [Int]) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        let xIndex = A.index(of: X)
//        if xIndex == nil {
//            return -1
//        }
//        
//        var missing = 1
//        
//        let sortedSet = Set(A).sorted()
//        for i in 0..<sortedSet.count {
//            if missing != sortedSet[i] {
//                return -1
//            }
//            missing += 1
//            if i+1 < sortedSet.count && sortedSet[i] != sortedSet[i+1] - 1 {
//                return -1
//            }
//        }
//        
//        if A.reduce(0, +) / A.count == X {
//            if A.count == 1 {
//                return 0
//            }
//            else {
//                return -1
//            }
//        }
//        
//        let set = Set(A)
//        var matching:[Int] = []
//        var clueIndex = 0
//        for e in A {
//            matching.append(e)
//            let matchingSet = Set(matching)
//            if matchingSet.count == set.count {
//                clueIndex = A.index(of: e)!
//                break
//            }
//        }
//        return max(clueIndex, xIndex!)
//    }
    
// MARK: Permutation Check
//    public func solution(_ A : inout [Int]) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        var missing = 1
//        let set = Set(A).sorted()
//        if set.count == A.count {
//            for i in 0..<set.count {
//                if missing != set[i] {
//                    return 0
//                }
//                missing += 1
//                if i+1 < set.count && set[i] != set[i+1] - 1 {
//                    missing = set[i+1] - 1
//                    return 0
//                }
//            }
//            return 1
//        }
//        else {
//            return 0
//        }
//    }
    
// MARK: Missing Integer
//    public func solution(_ A : inout [Int]) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        var missing:CLongLong = 1
//        let set = Set(A).sorted()
//        for e in set {
//            if missing != CLongLong(e) {
//                break
//            }
//            missing += 1
//        }
//        return Int(missing)
//    }
    
// MARK: TapeEquilibrium (Minimum Difference)
//    public func solution(_ A : inout [Int]) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        var minDif = 0
//        if A.count == 2 {
//            minDif = abs(A.first! - A.last!)
//        }
//        else {
//            var differences:[Int] = []
//            for i in 0..<A.count-1 {
//                var tempArray = A
//                for j in i+1..<A.count {
//                    tempArray[j] *= -1
//                }
//                differences.append(abs(tempArray.reduce(0, +)))
//            }
//            
//            minDif = differences.min()!
//        }
//        return minDif
//    }

// MARK: Missing Element
//    public func solution(_ A : inout [Int]) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        
//        let sorted = Set(A).sorted()
//        var missing = 1
//        for e in sorted {
//            if e != missing {
//                break
//            }
//            missing += 1
//        }
//        
//        return missing
//    }
    
// MARK: Equi
//    public func solution(_ A : inout [Int]) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        var sum:CLongLong = 0
//        var rsum:CLongLong = 0
//        var lsum:CLongLong = 0
//        var equi:Int = -1
//        
//        for i in 0..<A.count {
//            sum += A[i]
//        }
//        
//        for i in 0..<A.count {
//            rsum = sum - lsum - CLongLong(exactly: A[i])!
//            if rsum == lsum {
//                equi = i
//            }
//            lsum += A[i]
//        }
//        
//        return equi
//    }
    
// MARK: Frog Jump
//    public func solution(_ X : Int, _ Y : Int, _ D : Int) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        var times = 0
//        
//        if (Y - X) % D  == 0 {
//            times = (Y-X) / D
//        }
//        else {
//            times = (Y-X)/D + 1
//        }
//        
//        return times
//    }
    
// MARK: Binary Gap
//    public func solution(_ N : Int) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        var max_gap: UInt = 0
//        var current_gap: UInt = 0
//        
//        var n = UInt(exactly: N)!
//        
//        // division by 2 would produce each of bits right-to-left
//        // i.e. 1096 in bin is:
//        // 1 0 0 0 1 0 0 1 0 0 0
//        // first we divide until we skip all trailing zeroes
//        while n > 0 && n % 2 == 0 {
//            n /= 2
//        }
//        
//        while n > 0 {
//            // we divide first since last loop pretty much should have stumbled upon 1
//            n /= 2
//            
//            // reset counter and save new gap
//            if n % 2 == 1 {
//                if current_gap > max_gap {
//                    max_gap = current_gap
//                }
//                current_gap = 0
//            } else {
//                // bump counter if we stumble upon 0
//                current_gap += 1
//            }
//        }
//        
//        return Int(max_gap)
//    }
    
// MARK: Odd Occurrencies In Array
//    public func solution(_ A : inout [Int]) -> Int {
//        // write your code in Swift 3.0 (Linux)
//        setUniqueValue(array: A)
//        return uniqueValue
//    }
//    
//    var uniqueValue:Int = 0
//    
//    func setUniqueValue(array:[Int]) {
//        uniqueValue = array.first!
//        var tempArray = array
//        tempArray.removeFirst()
//        
//        for i in 1..<array.count {
//            if array[i] == uniqueValue {
//                tempArray.remove(at: i-1)
//                setUniqueValue(array: tempArray)
//                break
//            }
//        }
//    }
    
// MARK: Cyclic Rotations
//    public func solution(_ A : inout [Int], _ K : Int) -> [Int] {
//        // write your code in Swift 3.0 (Linux)
//        if A.count == 0 || A.count == 1 {
//            return A
//        }
//        
//        var shifts = K
//        while shifts >= A.count {
//            shifts -= A.count
//        }
//        
//        if shifts == 0 {
//            return A
//        }
//        
//        var tempArray = A
//        for i in 0..<shifts {
//            tempArray.insert(tempArray.last!, at: 0)
//            tempArray.removeLast()
//        }
//        return tempArray
//    }
}

